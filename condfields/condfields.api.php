<?php

/**
 * Implements hook_condfields().
 */
function hook_condfields() {
  $fields['node:page:full:field_images'] = array(
    'condition' => 'return innerWidth > 1000;',
  );

  return $fields;
}
