
(function($) {

  function findMarkers() {
    var markers = {}, found = 0;
    $('.condblocks-marker').each(function(i, el) {
      var $el = $(el),
          condition = $el.attr('data-condition'),
          cid = $el.attr('data-cid'),
          fn = new Function(condition);
      if (fn()) {
        markers[cid] = $el;
        found++;
      }
    });
    return found ? markers : false;
  }

  function loadBlocks(cids, callback) {
    var url = Drupal.settings.basePath + 'condblocks',
        query = 'cids[]=' + cids.join('&cids[]=');
    $.get(url + '?' + query).success(callback);
  }

  Drupal.behaviors.condblocks = {
    attach: function(context, settings) {

      var markers = findMarkers();
      if (markers) {
        var cids = Object.keys(markers);
        console.time && console.time('Loaded ' + cids.length + ' conditional blocks');

        var newEls = [];
        loadBlocks(cids, function(response) {
          console.timeEnd && console.timeEnd('Loaded ' + cids.length + ' conditional blocks');

          $.each(response.blocks, function(cid, html) {
            var $old = markers[cid],
                $new = $(html);
            $old.replaceWith($new);
            newEls.push($new[0]);
          });

          $.each(newEls, function(i, el) {
            Drupal.attachBehaviors(el, Drupal.settings);
          });
        });
      }

    }
  };

})(jQuery);
