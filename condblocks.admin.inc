<?php

/**
 * Form/page callback for admin/config/development/condblocks.
 */
function condblocks_config_form($form, &$form_state) {
  $form['condblocks_delay_resources'] = array(
    '#type' => 'checkbox',
    '#title' => check_plain(t('Delay JS & CSS loading')),
    '#default_value' => variable_get('condblocks_delay_resources', 0),
    '#description' => check_plain(t("Holds back block JS & CSS until the block is loaded. Better for performance, but probably breaks something.")),
  );

  $form['condblocks_slowness'] = array(
    '#type' => 'textfield',
    '#title' => t('Slowness'),
    '#default_value' => variable_get('condblocks_slowness', 0),
    '#element_validate' => array('element_validate_number'),
    '#size' => 3,
    '#field_suffix' => ' ' . t('seconds'),
    '#description' => check_plain(t("Makes the async block loading slower (server side), for debugging and styling purposes.")),
  );

  return system_settings_form($form);
}

/**
 * Page callback for admin/structure/blocks/condblocks.
 */
function condblocks_list_blocks() {
  $condblocks_vars = db_query("SELECT name FROM variable WHERE name LIKE 'condblocks\__%\_\__%'")->fetchCol();

  $items = array();
  foreach ($condblocks_vars as $name) {
    list($module, $delta) = explode('__', substr($name, 11));
    $items[] = l($module . ' : ' . $delta, "admin/structure/block/manage/$module/$delta/configure", array(
      'query' => array(
        'destination' => current_path(),
      ),
      'fragment' => 'edit-condblocks',
    ));
  }

  $title = t('Blocks from variables (manually remove if broken)');
  return theme('item_list', compact('items', 'title'));
}

/**
 * Implements hook_form_FORM_ID_alter() for block_admin_configure().
 */
function condblocks_form_block_admin_configure_alter(&$form, &$form_state) {
  $module = $form['module']['#value'];
  $delta = $form['delta']['#value'];
  $varname = str_replace('-', '_', 'condblocks_' . $module . '__' . $delta);

  $form['condblocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conditional blocks'),
    '#group' => 'visibility',
    '#weight' => 100,
  );
  $form['condblocks']['condblocks_condition'] = array(
    '#type' => 'textfield',
    '#title' => t('JS condition'),
    '#default_value' => variable_get($varname, ''),
    '#description' => t('One line of Javascript including <code>return</code> to indicate matching loading condition.'),
  );

  $form['#submit'][] = 'condblocks_block_admin_configure_submit';
}

/**
 * Submit handler for block_admin_configure().
 */
function condblocks_block_admin_configure_submit($form, &$form_state) {
  $module = $form_state['values']['module'];
  $delta = $form_state['values']['delta'];
  $varname = str_replace('-', '_', 'condblocks_' . $module . '__' . $delta);

  $condition = trim($form_state['values']['condblocks_condition']);
  if ($condition) {
    variable_set($varname, $condition);
  }
  else {
    variable_del($varname);
  }
}
