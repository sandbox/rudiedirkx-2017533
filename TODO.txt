To do

* Test with #prefix and #suffix (on outside element), because they come even later than #attached
  * Both in condblocks and condfields
* Do condblocks the condfields way: #pre_render, #theme_wrappers, #post_render, #attached
* condblocks' block_list/info_alter are useless? Just check the var at the very last second
